package hello;

import jdk.nashorn.internal.objects.NativeJSON;
import netscape.javascript.JSObject;
import org.apache.thrift.TException;

import java.util.ArrayList;
import java.util.List;

public class MyHelloService implements com.example.thrift.hello.HelloService.Iface {

    @Override
    public String hello(String name) throws TException {

        System.out.println("Received request with name = " + name);
        if (name.equals("Pizza")){
            return "I love PIZZA!";
        }else{
            return "Oh no! That's not right.";
        }
    }
}
