namespace java com.example.thrift.hello

service HelloService {
  string hello(1:string request);
}
